package co.edu.uniajc.turnhb.service;

import co.edu.uniajc.turnhb.model.servicioModel;
import co.edu.uniajc.turnhb.repository.ServicioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Service;
import java.util.List;
import java.util.Optional;

@Service
public class ServicioService {
    private final ServicioRepository servicioRepository;
    @Autowired
    public ServicioService(ServicioRepository servicioRepository) {
        this.servicioRepository = servicioRepository;
    }

    public servicioModel getById(int id) {
        return servicioRepository.getById(id);
    }

    @Query(nativeQuery = true, value = "SELECT" + "id" + ", servicio" + 
    		"FROM Servicio" + " WHERE id =: id")
    
    public List<servicioModel> findService(int id) {
        return servicioRepository.findService(id);
    }

    public List<servicioModel> findAll() {
        return servicioRepository.findAll();
    }

    public List<servicioModel> findAll(Sort sort) {
        return servicioRepository.findAll(sort);
    }

    public <S extends servicioModel> S save(S entity) {
        return servicioRepository.save(entity);
    }

    public Optional<servicioModel> findById(Long aLong) {
        return servicioRepository.findById(aLong);
    }

    public boolean existsById(Long aLong) {
        return servicioRepository.existsById(aLong);
    }

    public long count() {
        return servicioRepository.count();
    }

    public void deleteById(Long aLong) {
        servicioRepository.deleteById(aLong);
    }
    public servicioModel update(servicioModel servicioModel){
        if(existsById(servicioModel.getId()) == true)
        {
            servicioRepository.saveAndFlush(servicioModel);
        }
        return servicioModel;
    }
}

