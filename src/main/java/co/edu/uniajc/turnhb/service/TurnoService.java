package co.edu.uniajc.turnhb.service;

import co.edu.uniajc.turnhb.model.turnosModel;
import co.edu.uniajc.turnhb.repository.TurnosRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Service;
import java.util.List;
import java.util.Optional;

@Service
public class TurnoService {
    private final TurnosRepository turnosRepository;
    @Autowired
    public TurnoService(TurnosRepository turnosRepository) {
        this.turnosRepository = turnosRepository;
    }

    public turnosModel getById(int idTurno) {
        return turnosRepository.getById(idTurno);
    }

    @Query(nativeQuery = true, value = "SELECT" + "id" + ", celularCliente" + ", nombreCliente" + 
    		", apellidoCliente" + "tipoServicio" + "fechaTurno" + "horaTurno" + 
    		"FROM Turno" + " WHERE id =: id")
    
    public List<turnosModel> findEstado(Integer estado) {
        return turnosRepository.findEstado(estado);
    }

    public List<turnosModel> findAll() {
        return turnosRepository.findAll();
    }

    public List<turnosModel> findAll(Sort sort) {
        return turnosRepository.findAll(sort);
    }

    public <S extends turnosModel> S save(S entity) {
        return turnosRepository.save(entity);
    }

    public Optional<turnosModel> findById(Long aLong) {
        return turnosRepository.findById(aLong);
    }

    public boolean existsById(Long aLong) {
        return turnosRepository.existsById(aLong);
    }

    public long count() {
        return turnosRepository.count();
    }

    public void deleteById(Long aLong) {
        turnosRepository.deleteById(aLong);
    }
    public turnosModel update(turnosModel turnosModel){
        if(existsById(turnosModel.getIdTurno()) == true)
        {
            turnosRepository.saveAndFlush(turnosModel);
        }
        return turnosModel;
    }
}

