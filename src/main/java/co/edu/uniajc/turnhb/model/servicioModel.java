package co.edu.uniajc.turnhb.model;

//import lombok.Data;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "Servicios")
public class servicioModel implements Serializable{

	private static final long serialVersionUID = 1L;
	
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id")
    private long id;
    
    @Column(name="servicio")
    private String servicio;
    
    @Column(name="servicioDuracion")
    private boolean servicioDuracion;
    
    public servicioModel() {
    	//Constructor
    }
    
    public servicioModel(int id, String servicio, boolean servicioDuracion) {
        this.id = id;
        this.servicio = servicio;
        this.servicioDuracion = servicioDuracion;
    }
    
    public long getId() { return id; }
    public void setId(int id) { this.id = id; }

    public String getServicio() { return servicio; }
    public void setServicio(String servicio) { this.servicio = servicio; }
    
    public Boolean getServicioDuracion() { return servicioDuracion; }
    public void setServicioDuracion(Boolean servicioDuracion) { this.servicioDuracion = servicioDuracion; }
}
