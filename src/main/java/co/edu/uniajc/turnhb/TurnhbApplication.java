package co.edu.uniajc.turnhb;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TurnhbApplication {

	public static void main(String[] args) {
		SpringApplication.run(TurnhbApplication.class, args);
	}

}
