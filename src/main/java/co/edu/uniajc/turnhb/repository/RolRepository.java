package co.edu.uniajc.turnhb.repository;

import co.edu.uniajc.turnhb.model.rolModel;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface RolRepository extends JpaRepository<rolModel, Long>{
    List<rolModel> findAllByNamesContains(String rol);
    rolModel getById(int id);
    @Query(nativeQuery = true, value="SELECT"+
    "id"+",rol" + "FROM Rol"+" WHERE id =: id")
    List<rolModel> findRole(@Param(value = "id") Integer id);
}

